import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:date_time_picker/date_time_picker.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MyOwnLayout',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: ' The Win World Web Company'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 32,
                    backgroundImage: AssetImage('assets/images/pandas.JPG'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr. Vila Leng',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Software Developer',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.flutter_dash,
                    size: 69,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'Welcome To The WWC Official Account.',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: myController_user,
                  decoration: InputDecoration(
                    hintText: 'Username',
                    fillColor: Colors.blueGrey,
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30),
                child: TextField(
                  controller: myController_pwd,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    fillColor: Colors.blueGrey,
                    filled: true,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blueGrey, //color foreground
                      onPrimary: Colors.white, //color on click
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                      textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      myController_user.clear();
                      myController_pwd.clear();
                    },
                    child: Text('Cancel'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blueGrey, //color foreground
                      onPrimary: Colors.white, //color on click
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                      textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () => displayToast(),
                    //=> print('Hello Login!!!'),
                    child: Text('Login'),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
                child: TextField(
                  //controller: myController_pwd,
                  decoration: InputDecoration(
                    hintText: 'Please Contact Us If There Is Any Issues.',
                  ),
                ),
              ),
               Padding(
                padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
                child: Material(
                  elevation: 10,
                  child: DateTimePicker(
                    type: DateTimePickerType.dateTimeSeparate,
                    dateMask: 'd MMM, yyyy',
                    initialValue: DateTime.now().toString(),
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    icon: Icon(Icons.event),
                    dateLabelText: 'date',
                    timeLabelText: 'time',
                    onChanged: (val) => print (val),

                  ),
                )
              ),
            ],
          ),
        ),
      ),
    );
  } //end build

  void displayToast() {
    String username = myController_user.text;
    String password = myController_pwd.text;
    Fluttertoast.showToast(
      msg: 'Your username: $username \n Your Password: $password',
      toastLength: Toast.LENGTH_SHORT,
    );
  }
} //end class homepage
